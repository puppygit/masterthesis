import time;
import MySQLdb;
import random;
import datetime;
import Adafruit_BMP.BMP085 as BMP085;
import Adafruit_DHT;


count = 0; 
host = "localhost";
user = "raspberry";
password = "password";
database = "test";
# Sensor should be set to Adafruit_DHT.DHT11,
# Adafruit_DHT.DHT22, or Adafruit_DHT.AM2302.
DHT_TYPE = Adafruit_DHT.AM2302

# Example using a Raspberry Pi with DHT sensor
# connected to GPIO23.
DHT_PIN = 4


db = MySQLdb.connect(host, user, password, database);
curs=db.cursor();
sensor = BMP085.BMP085();

# Attempt to get sensor reading.
#humidity, temp = Adafruit_DHT.read(DHT_TYPE, DHT_PIN);


while (count<100000):
	try:
		barometerTemp=sensor.read_temperature();
		barometerPres=sensor.read_pressure();
		# Attempt to get sensor reading.
		humidity, temp = Adafruit_DHT.read(DHT_TYPE, DHT_PIN);
		timeStamp=str(datetime.datetime.now());
		
		fHumidity = float('{0:0.2f}'.format(humidity));
		fTempDHT = float('{0:0.2f}'.format(temp));
		fTemperature = float('{0:0.2f}'.format(barometerTemp));
		fPressure = float('{0:0.2f}'.format(barometerPres));
		mmPressure = fPressure*(760.0/101325);
		count = count+1;
		with open("Sensor.log", "a") as sensorLog:
			sensorLog.write("ID: ")
			sensorLog.write(str(count)+"\n")
			sensorLog.write("Timestamp: ")
			sensorLog.write(str(timeStamp)+"\n")
			sensorLog.write("Humidity: ")
			sensorLog.write(str(fHumidity)+"\n")
			sensorLog.write("DHT temperature: ")
			sensorLog.write(str(fTempDHT)+"\n")
			sensorLog.write("Barometer temperature: ")
			sensorLog.write(str(fTemperature)+"\n")
			sensorLog.write("Pressure: ")
			sensorLog.write(str(mmPressure)+"\n")
			sensorLog.write("Data Commited to the Database!\n")
			sensorLog.write("------------------------------------------------------\n")
		curs.execute("INSERT INTO rpi_data values (%s, %s, %s, %s, %s)", (count, timeStamp, fTemperature, fHumidity, mmPressure));
		db.commit();
		time.sleep(900);
	except:
		with open("Sensor.log", "a") as sensorLog:
			sensorLog.write("Error!\n")
		continue

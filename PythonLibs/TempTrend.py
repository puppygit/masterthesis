import MySQLdb;
import time;

count = 0; 
host = "localhost";
user = "raspberry";
password = "password";
database = "test";
sql = "SELECT temperature FROM rpi_data_test"
temperature = [];


db = MySQLdb.connect(host, user, password, database);
curs=db.cursor();
while (True):
	try:
		curs.execute("DROP TABLE trendTempData");
		curs.execute("CREATE TABLE trendTempData (id int auto_increment primary key, trendTempValue float)")
		curs.execute("DELETE FROM TrendtempFormula");
		curs.execute(sql)
		result_set = curs.fetchall()
		for row in result_set:
			temperature.append(row[0])
		N = len(temperature);
		x = range(N);
		B = (sum(x[i] * temperature[i] for i in xrange(N)) - 1./N*sum(x)*sum(temperature)) / (sum(x[i]**2 for i in xrange(N)) - 1./N*sum(x)**2)
		A = 1.*sum(temperature)/N - B * 1.*sum(x)/N
		trendVal = [A+B*val for val in temperature]
		trendFormula = str ("%f + %f * x" % (A, B))
		trendStr = [str(val) for val in trendVal];
		with open("TempData.log", "a") as sensorLog:
			sensorLog.write("Trend Line formula:"+trendFormula+"\n")
			sensorLog.write("------------------------------------------------------\n")
		curs.execute("INSERT INTO TrendtempFormula(tempFormula) VALUES(%s)", trendFormula);
		curs.executemany("INSERT INTO trendTempData(trendTempValue) VALUES(%s)", trendStr);
		db.commit();
		
		temperature = [];
		trendVal = [];
		trendStr = [];
		time.sleep(300);
	except:
		with open("TempData.log", "a") as sensorLog:
			sensorLog.write("Error!\n")
		continue

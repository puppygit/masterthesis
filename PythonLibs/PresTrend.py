import MySQLdb;
import time;

count = 0; 
host = "localhost";
user = "raspberry";
password = "password";
database = "test";
sql = "SELECT pressure FROM rpi_data_test"
pressure = [];

db = MySQLdb.connect(host, user, password, database);
curs=db.cursor();
while (True):
	try:
		curs.execute("DROP TABLE trendPresData");
		curs.execute("CREATE TABLE trendPresData (id int auto_increment primary key, trendPresValue float)");
		curs.execute("DELETE FROM TrendPresFormula");
		curs.execute(sql)
		result_set = curs.fetchall()
		for row in result_set:
			pressure.append(row[0])
		
		N = len(pressure);
		x = range(N);
		B = (sum(x[i] * pressure[i] for i in xrange(N)) - 1./N*sum(x)*sum(pressure)) / (sum(x[i]**2 for i in xrange(N)) - 1./N*sum(x)**2)
		A = 1.*sum(pressure)/N - B * 1.*sum(x)/N
		trendVal = [A+B*val for val in pressure]
		trendFormula = str ("%f + %f * x" % (A, B))
		trendStr = [str(val) for val in trendVal];
		with open("PressData.log", "a") as sensorLog:
			sensorLog.write("Trend Line formula:"+trendFormula+"\n")
			sensorLog.write("------------------------------------------------------\n")
		curs.execute("INSERT INTO TrendPresFormula(presFormula) VALUES(%s)", trendFormula);
		curs.executemany("INSERT INTO trendPresData(trendPresValue) VALUES(%s)", trendStr);
		db.commit();
		
		pressure = [];
		trendVal = [];
		trendStr = [];
		time.sleep(300);
	except:
		with open("PressData.log", "a") as sensorLog:
			sensorLog.write("Error!\n")
		continue
	

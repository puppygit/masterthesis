predTemp=[19.8;19.7;19.4;19.4;19.1;18.8;20.4;22.3];
data = iddata(predTemp,[], 900);
plot(data);
sys = ar(data,2);
K = 5;
p = forecast(sys,data,K);
plot(data,'b',p,'r'), legend('measured','forecasted'), grid on

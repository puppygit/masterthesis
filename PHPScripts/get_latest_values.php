<?php

/*
 * Following code will list all the products
 */

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

// get all products from products table
$result = mysql_query("SELECT temperature, humidity, pressure FROM rpi_data_test where id=(select MAX(id) from rpi_data_test)") or die(mysql_error());

// check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["values"] = array();
    
    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $product = array();
        $product["temperature"] = $row["temperature"];
        $product["humidity"] = $row["humidity"];
        $product["pressure"] = $row["pressure"];
        



        // push single product into final response array
        array_push($response["values"], $product);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No values found";

    // echo no users JSON
    echo json_encode($response);
}
?>

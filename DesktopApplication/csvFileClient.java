/**
 * 
 */
package masterThesis;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;

/**
 * @author Sergii
 *
 */
public class csvFileClient {
	//Save Data Table to CSV File.
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://192.168.0.14:3306/test";	 
	static final String USER = "raspberry";
	static final String PASS = "password";	
	
	public void saveDataAsCSV (){
			String filename ="C:\\RPIMonitor\\Save\\DataTable.csv";
		        try {
		            FileWriter fw = new FileWriter(filename);
		            try{
		            	Class.forName("com.mysql.jdbc.Driver");
		        		//STEP 3: Open a connection
		        		System.out.println("Connecting to database...");
		        		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		        		System.out.println("Creating statement...");
		        		stmt = conn.createStatement();
		        		String sql;
		        		sql = "SELECT timestamp, temperature, humidity, pressure FROM rpi_data_test";   
		        		rs = stmt.executeQuery(sql);
		        		}
		        	catch (Exception e) {
		        		e.printStackTrace();
		        		JOptionPane.showMessageDialog(null, "Failed to connect to Database. Please check if Database is running");
		        		}
		            fw.append("Timestamp");
		            fw.append(';');
		    		fw.append("Temperature");
		    		fw.append(';');
		    		fw.append("Humidity");
		    		fw.append(';');
		    		fw.append("Pressure");
		    		fw.append(";\n");
		    		
		            while (rs.next()) {
		                fw.append(rs.getString(1));
		                fw.append(';');
		                fw.append(rs.getString(2));
		                fw.append(';');
						fw.append(rs.getString(3));
		                fw.append(';');
		                fw.append(rs.getString(4));
		                fw.append(";\n");
		               }
		            fw.flush();
		            fw.close();
		            conn.close();
		            System.out.println("CSV File is created successfully.");
		            JOptionPane.showMessageDialog(null, "CSV File is created successfully.");
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    
		}

}

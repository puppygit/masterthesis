/**
 * 
 */
package masterThesis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 * @author Sergii
 *
 */
public class EmailClient {
	Connection conn = null;
	
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://192.168.0.14:3306/test";	 
	static final String USER = "raspberry";
	static final String PASS = "password";
	
	public void sendNotificationMessage (String msgText, String msgSubject) {
		
		final String uName = "rpi.email4tests@gmail.com";
		final String gPassword = "raspberry2";
		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(uName, gPassword);
			}
		});
		try {
			Message message = new MimeMessage (session);
			message.setFrom(new InternetAddress("rpi.email4tests@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("s.ignatov92@gmail.com"));
			message.setSubject(msgSubject);
			message.setContent(msgText, "text/html; charset=utf-8");
			
			Transport.send(message);
			//JOptionPane.showMessageDialog(null, "Email sent successfully!");
			System.out.println("Notification Sent!");
		}
		catch (MessagingException mesEx){
			mesEx.printStackTrace();
			Logger.getLogger(EmailClient.class.getName()).log(Level.SEVERE,"", mesEx);
			JOptionPane.showMessageDialog(null, "Eror Sending Notification");
		}
	}
	public void dbConnect () throws SQLException, ClassNotFoundException {
		//STEP 2: Register JDBC driver
		Class.forName("com.mysql.jdbc.Driver");
		//STEP 3: Open a connection
		System.out.println("Connecting to database...");
		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		
	}
	public void sendCurrentValues() throws ClassNotFoundException, SQLException {
		String strTempValue = null;
		String strHumValue = null;
		String strPressValue = null;
		dbConnect();
		Statement stmt_curr = conn.createStatement();
		String sql_curr = "SELECT temperature, humidity, pressure  FROM rpi_data_test ORDER BY timestamp DESC LIMIT 1;";   
		ResultSet rs_curr = stmt_curr.executeQuery(sql_curr);
		while (rs_curr.next()){
			//Current temperature
			strTempValue = rs_curr.getString("temperature");
			// Current Humidity
			strHumValue = rs_curr.getString("humidity");
			// Current Pressure
			strPressValue = rs_curr.getString("pressure");
		}
		conn.close();
		String emailMsg = "Current Temperature: "+strTempValue+" *C\n"+"Current Humidity: "+strHumValue+" %\n"+"Current Pressure: "+strPressValue+" mmHg\n";
		sendNotificationMessage(emailMsg, "Current Values");
	}
	public void checkCurrentValues () throws ClassNotFoundException, SQLException {
		dbConnect();
		Statement stmt_curr = conn.createStatement();
		String sql_curr = "SELECT temperature, humidity, pressure  FROM rpi_data_test ORDER BY timestamp DESC LIMIT 1;";   
		ResultSet rs_curr = stmt_curr.executeQuery(sql_curr);
		while (rs_curr.next()){
			//Current temperature
			float tempValue = rs_curr.getFloat("temperature");
			String strTempValue = rs_curr.getString("temperature");
			if ((tempValue>=24.0)&&(tempValue<30.0)){
				sendNotificationMessage("WARNING! Temperature is higher than normal. Please turn on Air Conditioner!\nCurrent Value of temperature is: "+strTempValue, "[WARNING] Temperature is higher than NORMAL");
			} else if (tempValue>=30.0){
				sendNotificationMessage("CRITICAL! Temperature is CRITICAL. Please turn on Air Conditioner!\nCurrent Value of temperature is: "+strTempValue, "[CRITICAL] Temperature is at CRITICAL level");
			} else if ((tempValue<20.0)&&(tempValue>=18.0)){
				sendNotificationMessage("WARNING! Temperature is lower than normal. Please turn on Air Conditioner!\nCurrent Value of temperature is: "+strTempValue, "[WARNING] Temperature is higher than NORMAL");
			} else if ((tempValue<18.0)&&(tempValue>=12.0)){
				sendNotificationMessage("CRITICAL! Temperature is CRITICAL. Please turn on Air Conditioner!\nCurrent Value of temperature is: "+strTempValue, "[CRITICAL] Temperature is at CRITICAL level");
			} else if (tempValue<12.0){
				sendNotificationMessage("DANGER! Temperature is too low. Please turn on Air Conditioner!\nCurrent Value of temperature is: "+strTempValue, "[DANGER] Temperature is at DANGEROUS level");
			} else {
				System.out.println("temperature value ok");
			}
			
			// Current Humidity
			float humValue = rs_curr.getFloat("humidity");
			String strHumValue = rs_curr.getString("humidity");
			if ((humValue<30.0)&&(humValue>=20.0)){
				sendNotificationMessage("WARNING! Humidity is lower than normal. Please turn on Humidifier!\nCurrent Value of humidity is: "+strHumValue, "[WARNING] Humidity is lower than NORMAL");
			} else if (humValue<20.0){
				sendNotificationMessage("CRITICAL! Humidity is CRITICAL. Please turn on Humidifier!\nCurrent Value of humidity is: "+strHumValue, "[CRITICAL] Humidity is at CRITICAL level");
			} else if ((humValue>50.0)&&(humValue<=60.0)){
				sendNotificationMessage("WARNING! Humidity is higher thank normal. \nCurrent Value of humidity is: "+strHumValue, "[CRITICAL] Humidity is higher than NORMAL");
			} else if (humValue>60.0){
				sendNotificationMessage("CRITICAL! Humidity is CRITICAL.\nCurrent Value of humidity is: "+strHumValue, "[CRITICAL] Humidity is at CRITICAL level");
			}
			else {
				System.out.println("humidity value ok");
			}
			
			
		}
		conn.close();
	}
}

/**
 * 
 */
package masterThesis;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Vector;

import javax.swing.JOptionPane;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author Sergii
 *
 */
public class pdfClient {
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://192.168.0.14:3306/test";	 
	static final String USER = "raspberry";
	static final String PASS = "password";
	
	@SuppressWarnings("rawtypes")
	Vector colsNames = new Vector();
	@SuppressWarnings("rawtypes")
	Vector data = new Vector();

	
	public void dbConnect () throws SQLException, ClassNotFoundException {
		//STEP 2: Register JDBC driver
		Class.forName("com.mysql.jdbc.Driver");
		//STEP 3: Open a connection
		System.out.println("Connecting to database...");
		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		
	}
	//SQL query for data table
	public void selectAll () throws ClassNotFoundException, SQLException{
		
		dbConnect();
		System.out.println("Creating statement...");
		stmt = conn.createStatement();
		String sql;
		sql = "SELECT timestamp, temperature, humidity, pressure FROM rpi_data_test";   
		rs = stmt.executeQuery(sql);
	}
	public void generateReport(){
		//TBD
		Statement st = null;
		ResultSet rs_rep = null;
		Statement st_f = null;
		ResultSet rs_f = null;
		ResultSet rs_h = null;
		Statement st_h = null;
		ResultSet rs_p = null;
		Statement st_p = null;
		
		try{
			Document report = new Document();
			String fname = "C:\\RPIMonitor\\Save\\Report.pdf";
			try{
				dbConnect();
				st = conn.createStatement();
				String sql_rep = "SELECT temperature, humidity, pressure  FROM rpi_data_test ORDER BY timestamp DESC LIMIT 1;";
				rs_rep = st.executeQuery(sql_rep); 
				}
			catch (Exception e) {
	        	e.printStackTrace();
	        	JOptionPane.showMessageDialog(null, "Failed to connect to Database. Please check if Database is running");
	        	}	
			PdfWriter.getInstance(report, new FileOutputStream(fname));
			report.open();
			report.add(new Paragraph("Current Values"));
			report.add(new Paragraph(new Date().toString()));
	        report.add(new Paragraph("\n"));
			PdfPTable table = new PdfPTable(3);
	        PdfPCell cell = new PdfPCell(new Paragraph("Current Values"));
	        cell.setColspan(3);
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell.setBackgroundColor(BaseColor.GREEN);
	        
	        
	      //Write data to Table
    		int column = 3;
    		table.addCell(cell);
        	table.addCell("temperature, *C");
        	table.addCell("humidity, %");
        	table.addCell("pressure, mmHg");
        	
        	while(rs_rep.next()){
    			//Vector row = new Vector(column);
    			for(int i=1; i<=column; i++) {
    	            //row.addElement(rs.getObject(i));
    	            table.addCell(rs_rep.getString(i));
    	        }	   
        	}
        	report.add(table);
        	
        	report.add(new Paragraph("\n"));
        	
        	try{
				dbConnect();
				st_f = conn.createStatement();
				String sql_f = "SELECT tempFormula  FROM TrendtempFormula;";
				rs_f = st_f.executeQuery(sql_f); 
				st_h = conn.createStatement();
				String sql_h = "SELECT humFormula  FROM TrendhumFormula;";
				rs_h = st_h.executeQuery(sql_h);
				st_p = conn.createStatement();
				String sql_p = "SELECT presFormula  FROM TrendPresFormula;";
				rs_p = st_p.executeQuery(sql_p);
				}
			catch (Exception e) {
	        	e.printStackTrace();
	        	JOptionPane.showMessageDialog(null, "Failed to connect to Database. Please check if Database is running");
	        	}	
        	while(rs_f.next()){
        		report.add(new Paragraph("Temperature Trend Formula"));
        		report.add(new Paragraph(rs_f.getString("tempFormula")));
        		report.add(new Paragraph("\n"));
        	}
        	while(rs_h.next()){
        		report.add(new Paragraph("Humidity Trend Formula"));
        		report.add(new Paragraph(rs_h.getString("humFormula")));
        		report.add(new Paragraph("\n"));
        	}
        	while(rs_p.next()){
        		report.add(new Paragraph("Pressure Trend Formula"));
        		report.add(new Paragraph(rs_p.getString("presFormula")));
        		report.add(new Paragraph("\n"));
        	}
        	
        	report.close();
        	JOptionPane.showMessageDialog(null, "PDF File is created successfully.");

			}
		catch (Exception e) {
        	e.printStackTrace();
        	JOptionPane.showMessageDialog(null, e);
		}
	}
	public void saveDataTableToPDF() throws ClassNotFoundException, FileNotFoundException, DocumentException {
        try{
        	
        	Document document = new Document();
        	String filename = "C:\\RPIMonitor\\Save\\DataTable.pdf";
        	try{
        		selectAll();
        		}
        	catch (Exception e) {
        		e.printStackTrace();
        		JOptionPane.showMessageDialog(null, "Failed to connect to Database. Please check if Database is running");
        		}
        	colsNames.clear();
    		data.clear();
    		
    		
        	PdfWriter.getInstance(document, new FileOutputStream(filename));
        	document.open();
        	document.add(new Paragraph("Sensor Data"));
        	document.add(new Paragraph(new Date().toString()));
        	PdfPTable table = new PdfPTable(4);
        	PdfPCell cell = new PdfPCell(new Paragraph("Sensor Data"));
        	cell.setColspan(4);
        	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	cell.setBackgroundColor(BaseColor.GREEN);
        	
    		
    		//Write data to Table
    		int column = 4;
    		table.addCell(cell);
        	table.addCell("timestamp");
        	table.addCell("temperature, *C");
        	table.addCell("humidity, %");
        	table.addCell("pressure, mmHg");
    		
        	while(rs.next()){
    			//Vector row = new Vector(column);
    			for(int i=1; i<=column; i++) {
    	            //row.addElement(rs.getObject(i));
    	            table.addCell(rs.getString(i));
    	        }	   
        	}
        	//Write data to document
        	document.add(table);
        	document.close();
        	JOptionPane.showMessageDialog(null, "PDF File is created successfully.");
        }
        catch (Exception e) {
        	e.printStackTrace();
        	JOptionPane.showMessageDialog(null, e);
        	
        }
	}
}

/**
 * 
 */
package masterThesis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleConstants.FontConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.jdbc.JDBCCategoryDataset;
import org.jfree.data.xy.XYSeries;

import com.itextpdf.text.DocumentException;

/**
 * @author Sergii
 *
 */
@SuppressWarnings("serial")
public class AppWindow extends JFrame {
	
	
	//UI Elements initialization
	public JPanel dataTab = new JPanel();
	public JToolBar toolBar = new JToolBar();
	public JButton btnRefreshChart = new JButton("Refresh Chart");
	public JButton btnSendCurrent = new JButton("Send Current Values");
	public JButton btnRefreshDataTable = new JButton("Refresh Data Table");
	public JButton btnSaveAsCsv = new JButton("");
	public JButton btnSaveAsPdf = new JButton("");
	public JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	public JPanel graphsTab = new JPanel();
	public JScrollPane scrollPane = new JScrollPane();
	public JPanel dataTablePanel = new JPanel();
	public JPanel graphsPanel = new JPanel();
	public JTextPane humidityPane = new JTextPane();
	public JLabel lblHumidity = new JLabel("Humidity, %");
	public JTextPane pressurePane = new JTextPane();
	public JLabel lblPressure = new JLabel("Pressure, mmHg");
	public JLabel lblTemperature = new JLabel("Temperature, *C");
	public JTextPane temperaturePane = new JTextPane();
	public JRadioButton rdbtnTemperature = new JRadioButton("Temperature");
	public JRadioButton rdbtnHumidity = new JRadioButton("Humidity");
	public JRadioButton rdbtnPressure = new JRadioButton("Pressure");
	public JButton btnAbout = new JButton("About");
	//  Database credentials
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://192.168.0.14:3306/test";	 
	static final String USER = "raspberry";
	static final String PASS = "password";	
	
	//variables declaration
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	DefaultTableModel model;
	@SuppressWarnings("rawtypes")
	Vector colsNames = new Vector();
	@SuppressWarnings("rawtypes")
	Vector data = new Vector();
	XYSeries chartData = new XYSeries("Temperature");
	boolean tableIsShown = true;
	public String date, temperature;
	private JTable table;
	boolean aboutShown = false;
	private final JButton btnPdfReport = new JButton("PDF Report");
	
	
	
	public AppWindow() throws InterruptedException, ClassNotFoundException, SQLException {
		setResizable(false);
		
		InitClass.mailClient = new EmailClient();
		InitClass.csvClient = new csvFileClient();
		InitClass.pdf_Client = new pdfClient();
		
		
		setForeground(new Color(255, 250, 240));
		setSize(new Dimension(970, 515));
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\app_icon_v0.png"));
		setVisible(true);
		
		getContentPane().add(toolBar, BorderLayout.NORTH);
		btnRefreshDataTable.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\table_refresh.png"));
		toolBar.add(btnRefreshDataTable);
		//Mouse event handler to show data on temperature
		btnRefreshDataTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mev) {
				
				//System.out.println("Showing Data table");
				getData();
				
			}
		});
		btnRefreshChart.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\chartf5.jpg"));
		btnRefreshChart.setToolTipText("Refresh Current Chart");
		toolBar.add(btnRefreshChart);
		btnSendCurrent.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\Email-icon_16x16.png"));
		
		btnSendCurrent.setToolTipText("Sends Current Values to email");
		toolBar.add(btnSendCurrent);
		btnPdfReport.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\report-3-xxl.png"));
		
		
		toolBar.add(btnPdfReport);
		btnAbout.setName("About");
		btnAbout.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\get_info.png"));
		
		
		toolBar.add(btnAbout);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		dataTab.setBounds(new Rectangle(1, 1, 1, 1));
		dataTab.setSize(new Dimension(600, 400));
		tabbedPane.addTab("Data table", new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\dataTabIcon_24x24.png"), dataTab, "View data table");
		dataTab.setLayout(null);
		
		//Test Style for Current values
		SimpleAttributeSet attribs = new SimpleAttributeSet();  
		StyleConstants.setAlignment(attribs , StyleConstants.ALIGN_CENTER);  
		FontConstants.setFontFamily(attribs, "Microsoft JhengHei UI");
		FontConstants.setFontSize(attribs, 18);
		StyleConstants.setBold(attribs, true);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setBounds(120, 0, 11, 450);
		dataTab.add(verticalStrut);
		btnSaveAsCsv.setSize(new Dimension(32, 32));
		btnSaveAsCsv.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSaveAsCsv.setToolTipText("Save as CSV");
		btnSaveAsCsv.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\csv-icon_32x32.png"));
		btnSaveAsCsv.setBounds(40, 245, 33, 32);
		dataTab.add(btnSaveAsCsv);
		btnSaveAsCsv.setFont(new Font("Dialog", Font.BOLD, 10));
		btnSaveAsPdf.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSaveAsPdf.setToolTipText("Save as PDF");
		btnSaveAsPdf.setIcon(new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\pdf-icon_32x32.png"));
		btnSaveAsPdf.setBounds(40, 307, 32, 32);
		dataTab.add(btnSaveAsPdf);
		temperaturePane.setToolTipText("Current Value of Temperature");
		
		temperaturePane.setEditable(false);
		
		//JTextPane temperaturePane = new JTextPane();
		temperaturePane.setBorder(UIManager.getBorder("CheckBox.border"));
		temperaturePane.setBackground(new Color(102, 255, 153));
		temperaturePane.setBounds(4, 27, 104, 40);
		temperaturePane.setParagraphAttributes(attribs, true);
		temperaturePane.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 18));
		dataTab.add(temperaturePane);
		
		//JLabel lblTemperature = new JLabel("Temperature");
		lblTemperature.setBounds(8, 12, 96, 16);
		dataTab.add(lblTemperature);
		
		humidityPane.setToolTipText("Current Value of Humidity");
		humidityPane.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 18));
		humidityPane.setEditable(false);
		humidityPane.setBorder(UIManager.getBorder("CheckBox.border"));
		humidityPane.setBounds(4, 94, 104, 40);
		humidityPane.setBackground(new Color(102, 255, 153));
		humidityPane.setParagraphAttributes(attribs, true);
		dataTab.add(humidityPane);
		lblHumidity.setBounds(21, 79, 71, 16);
		dataTab.add(lblHumidity);
		
		pressurePane.setToolTipText("Current Value of Pressure");
		pressurePane.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 18));
		pressurePane.setEditable(false);
		pressurePane.setBounds(4, 163, 104, 40);
		pressurePane.setBorder(UIManager.getBorder("CheckBox.border"));
		pressurePane.setBackground(new Color(102, 255, 153));
		pressurePane.setParagraphAttributes(attribs, true);
		dataTab.add(pressurePane);
		lblPressure.setBounds(4, 146, 104, 16);
		
		dataTab.add(lblPressure);
		
		
		
		//JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(130,0,830,450);
		
		table = new JTable();
		
		tabbedPane.addTab("Graphs", new ImageIcon("C:\\Users\\Sergii\\Documents\\workspace\\MasterThesisPRJ\\Icons\\Actions-office-chart-line-stacked-icon.png"), graphsTab, "View Graphs");
		graphsTab.setLayout(null);
		
		
		
		graphsPanel.setBounds(124, 12, 813, 400);
		graphsPanel.setLayout(null);
		graphsTab.add(graphsPanel);
		
		
		JButton btnShowChart = new JButton("Save Chart");
		btnShowChart.setToolTipText("Save current chart as PNG");
		
		
		btnShowChart.setBounds(12, 12, 100, 44);
		graphsTab.add(btnShowChart);
		
		
		//rdbtnTemperature.setSelected(true);
		rdbtnTemperature.setBounds(8, 91, 121, 24);
		rdbtnTemperature.setSelected(true);
		graphsTab.add(rdbtnTemperature);
		
		
		rdbtnHumidity.setBounds(8, 131, 121, 24);
		graphsTab.add(rdbtnHumidity);
		
		
		rdbtnPressure.setBounds(8, 172, 121, 24);
		graphsTab.add(rdbtnPressure);
		//--------------------------------------
		
		//App window properties
		this.setTitle("RPI Monitor");
		this.setSize(970, 546);
		this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter() {
            // windowsClosing when the X button is clicked.
            @Override
            public void windowClosing(WindowEvent e) {
                exitApplication();
            }
        });
        btnSendCurrent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					InitClass.mailClient.sendCurrentValues();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
        btnPdfReport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				InitClass.pdf_Client.generateReport();
			}
		});
        btnSaveAsPdf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					InitClass.pdf_Client.saveDataTableToPDF();
				} catch (ClassNotFoundException | FileNotFoundException | DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSaveAsCsv.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mev) {
				InitClass.csvClient.saveDataAsCSV();
			}
		});
		
		btnShowChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					saveChartAsPNG();
					JOptionPane.showMessageDialog(null, "Chart Saved in default location");
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		rdbtnTemperature.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rdbtnHumidity.setSelected(false);
				rdbtnPressure.setSelected(false);
				rdbtnTemperature.setSelected(true);
				try {
					temperatureChart();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		rdbtnHumidity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rdbtnHumidity.setSelected(true);
				rdbtnPressure.setSelected(false);
				rdbtnTemperature.setSelected(false);
				try {
					humidityChart();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		rdbtnPressure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rdbtnHumidity.setSelected(false);
				rdbtnPressure.setSelected(true);
				rdbtnTemperature.setSelected(false);
				try {
					pressureChart();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JOptionPane.showMessageDialog(null,"Room temperature, humidity and pressure monitor\n"+
													"Version 1.1\n"+
													"Assembled on Raspberry Pi 2 Model B\n"+
													"Used Sensors:\n"+
													"Barometer: Adafruit BMP180\n"+
													"Temperature and humidity Sensor: Adafruit DHT2302\n"+
													"This software is free.\n\n"+
													"(c)Sergii Ignatov\n"+
													"Department of Computer Control\n"+
													"Tallinn University of Techology, 2016\n","About RPI Monitor",JOptionPane.INFORMATION_MESSAGE);
			}
		});
        
		
		
		getData();
        temperatureChart();
        
        while(tableIsShown){
        	//InitClass.mailClient.checkCurrentValues();
        	displayCurrentValues();
        	graphsPanel.repaint();
        	Thread.sleep(60000);
        }
        

	}
	//Show data table routine
	public void getData () {
		try{
			
			reloadData();
			model = new DefaultTableModel(data, colsNames);
			table.setModel(model);
			dataTab.add(scrollPane);
			scrollPane.setViewportView(table);
			
		}catch(Exception e) {
	        e.printStackTrace();
	    }
	}
	//Connection to Database 
	public void dbConnect () throws SQLException, ClassNotFoundException {
		//STEP 2: Register JDBC driver
		Class.forName("com.mysql.jdbc.Driver");
		//STEP 3: Open a connection
		System.out.println("Connecting to database...");
		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		
	}
	//SQL query for data table
	public void selectAll () throws ClassNotFoundException, SQLException{
		
		dbConnect();
		System.out.println("Creating statement...");
		stmt = conn.createStatement();
		String sql;
		sql = "SELECT timestamp, temperature, humidity, pressure FROM rpi_data_test";   
		rs = stmt.executeQuery(sql);
	}
	//Data Table routine
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void reloadData () throws ClassNotFoundException, SQLException {
		try{
		selectAll();
		}
		catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Failed to connect to Database. Please check if Database is running");
		}
		colsNames.clear();
		data.clear();
		//STEP 4: Execute a query
		ResultSetMetaData rsmd = rs.getMetaData();
		int column = rsmd.getColumnCount();
		colsNames.addElement("Timestamp");
		colsNames.addElement("Temperature, *C");
		colsNames.addElement("Humidity, %");
		colsNames.addElement("Pressure, mmHg");

		while(rs.next()){
			Vector row = new Vector(column);
			for(int i=1; i<=column; i++) {
	            row.addElement(rs.getObject(i));
	        }
	        data.addElement(row);
			   
		    }
	}
		//Method to display the last values from DB
	//Color of the area varies depending on the value of the variable.
	//TODO - Set the levels. Research on google needed.
	public void displayCurrentValues() throws ClassNotFoundException, SQLException{
		dbConnect();
		Statement stmt_curr = conn.createStatement();
		String sql_curr = "SELECT temperature, humidity, pressure  FROM rpi_data_test ORDER BY timestamp DESC LIMIT 1;";   
		ResultSet rs_curr = stmt_curr.executeQuery(sql_curr);
		while (rs_curr.next()){
			//Current temperature
			float tempValue = rs_curr.getFloat("temperature");
			String strTempValue = rs_curr.getString("temperature");
			if ((tempValue>=24.0)&&(tempValue<30.0)){
				temperaturePane.setBackground(new Color(255, 204, 0));
			} else if (tempValue>=30){
				temperaturePane.setBackground(new Color(255, 51, 51));
			} else if ((tempValue<20.0)&&(tempValue>=18.0)){
				temperaturePane.setBackground(new Color(255, 204, 0));
			} else if (tempValue<18.0){
				temperaturePane.setBackground(new Color(255, 51, 51));
			} else {
				temperaturePane.setBackground(new Color(102, 255, 153));
			}
			temperaturePane.setText(strTempValue);
			// Current Humidity
			float humValue = rs_curr.getFloat("humidity");
			String strHumValue = rs_curr.getString("humidity");
			if ((humValue<30.0)&&(humValue>=20.0)){
				humidityPane.setBackground(new Color(255, 204, 0));
			} else if (humValue<20.0){
				humidityPane.setBackground(new Color(255, 51, 51));
			} else if ((humValue>50.0)&&(humValue<=60.0)){
				humidityPane.setBackground(new Color(255, 204, 0));
			} else if (humValue>60.0){
				humidityPane.setBackground(new Color(255, 51, 51));
			} else {
				humidityPane.setBackground(new Color(102, 255, 153));
			}
			humidityPane.setText(strHumValue);
			String strPressValue = rs_curr.getString("pressure");
			pressurePane.setText(strPressValue);
			
		}
		conn.close();
	}
	
	
	//Chart saving as PNG routine
	public void saveChartAsPNG () throws ClassNotFoundException, SQLException {
		if (rdbtnPressure.isSelected()) {
			dbConnect();
			stmt = conn.createStatement();
			String sqlPressure = "SELECT timestamp, pressure FROM rpi_data_test WHERE id>=((SELECT MAX(id) FROM rpi_data_test)-40) ORDER BY id ASC;";   
			String sqlTrendPres = "select rpi_data_test.timestamp, trendPresData.trendPresValue from rpi_data_test inner join trendPresData on rpi_data_test.id=trendPresData.id;";
			JDBCCategoryDataset datasetPres = new JDBCCategoryDataset(conn, sqlPressure);
			JDBCCategoryDataset datasetTrendPres = new JDBCCategoryDataset(conn, sqlTrendPres);
			JFreeChart pressChart = ChartFactory.createLineChart("Atmospheric pressure", "Timestamp", "Pressure, mmHg", datasetPres, PlotOrientation.VERTICAL, true, true, true);
			pressChart.getTitle().setPaint(Color.BLUE);
			CategoryPlot plot = pressChart.getCategoryPlot();
			CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();
			ValueAxis yAxis = plot.getRangeAxis();
			yAxis.setRange(700, 800);
			xAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
			plot.setRangeGridlinePaint(Color.blue);
			plot.setDomainGridlinesVisible(true);
			plot.setDomainGridlinePaint(Color.BLUE);
			LineAndShapeRenderer renderer = (LineAndShapeRenderer)plot.getRenderer();
			renderer.setBaseShapesVisible(true);
			
			plot.setDataset(1, datasetTrendPres);
			LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(true, false);
			renderer2.setSeriesPaint(0, Color.GREEN);
			renderer2.setBaseShapesVisible(false);
			plot.setRenderer(1, renderer2);
			
			
			try {
				final ChartRenderingInfo chartInfo = new ChartRenderingInfo(new StandardEntityCollection());
				final File pngChart = new File("C:\\RPIMonitor\\Save\\PressureChart.png");
				ChartUtilities.saveChartAsPNG(pngChart, pressChart, graphsPanel.getWidth(), graphsPanel.getHeight(), chartInfo);
			}
			catch (Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Can't create PNG image of Chart.");
			}
			conn.close();
		} else if (rdbtnHumidity.isSelected()){
			dbConnect();
			stmt = conn.createStatement();
			String sqlHum = "SELECT timestamp, humidity FROM rpi_data_test WHERE id>=((SELECT MAX(id) FROM rpi_data_test)-40) ORDER BY id ASC;";   
			String sqlTrendHum = "select rpi_data_test.timestamp, trendHumData.trendHumValue from rpi_data_test inner join trendHumData on rpi_data_test.id=trendHumData.id;";
			JDBCCategoryDataset datasetHum = new JDBCCategoryDataset(conn, sqlHum);
			JDBCCategoryDataset datasetTrendHum = new JDBCCategoryDataset(conn,sqlTrendHum);
			JFreeChart humChart = ChartFactory.createLineChart("Humidity", "Timestamp", "Humidity, %", datasetHum, PlotOrientation.VERTICAL, true, true, true);
			humChart.getTitle().setPaint(Color.BLUE);
			CategoryPlot plot = humChart.getCategoryPlot();
			CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();
			xAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
			plot.setRangeGridlinePaint(Color.blue);
			plot.setDomainGridlinesVisible(true);
			plot.setDomainGridlinePaint(Color.BLUE);
			LineAndShapeRenderer renderer = (LineAndShapeRenderer)plot.getRenderer();
			renderer.setBaseShapesVisible(true);
			
			plot.setDataset(1, datasetTrendHum);
			LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(true, false);
			renderer2.setSeriesPaint(0, Color.GREEN);
			renderer2.setBaseShapesVisible(false);
			plot.setRenderer(1, renderer2);
			
			try {
				final ChartRenderingInfo chartInfo = new ChartRenderingInfo(new StandardEntityCollection());
				final File pngChart = new File("C:\\RPIMonitor\\Save\\HumidityChart.png");
				ChartUtilities.saveChartAsPNG(pngChart, humChart, graphsPanel.getWidth(), graphsPanel.getHeight(), chartInfo);
			}
			catch (Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Can't create PNG image of Chart.");
			}
			conn.close();
		} else if (rdbtnTemperature.isSelected()){
			dbConnect();
			stmt = conn.createStatement();
			String sqlTemp = "SELECT timestamp, temperature FROM rpi_data_test WHERE id>=((SELECT MAX(id) FROM rpi_data_test)-40) ORDER BY id ASC;";   
			String sqlTrendTemp = "select rpi_data_test.timestamp, trendTempData.trendTempValue from rpi_data_test inner join trendTempData on rpi_data_test.id=trendTempData.id;";
			JDBCCategoryDataset datasetTemp = new JDBCCategoryDataset(conn, sqlTemp);
			JDBCCategoryDataset datasetTrendTemp = new JDBCCategoryDataset(conn, sqlTrendTemp);
			JFreeChart tempChart = ChartFactory.createLineChart("Temperature", "Timestamp", "Temperature, *C", datasetTemp, PlotOrientation.VERTICAL, true, true, true);
			tempChart.getTitle().setPaint(Color.BLUE);
			CategoryPlot plot = tempChart.getCategoryPlot();
			CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();
			xAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
			plot.setRangeGridlinePaint(Color.blue);
			plot.setDomainGridlinesVisible(true);
			plot.setDomainGridlinePaint(Color.BLUE);
			LineAndShapeRenderer renderer = (LineAndShapeRenderer)plot.getRenderer();
			renderer.setBaseShapesVisible(true);
			
			plot.setDataset(1, datasetTrendTemp);
			LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(true, false);
			renderer2.setSeriesPaint(0, Color.GREEN);
			renderer2.setBaseShapesVisible(false);
			plot.setRenderer(1, renderer2);
			
			
			try {
				final ChartRenderingInfo chartInfo = new ChartRenderingInfo(new StandardEntityCollection());
				final File pngChart = new File("C:\\RPIMonitor\\Save\\TemperatureChart.png");
				ChartUtilities.saveChartAsPNG(pngChart, tempChart, graphsPanel.getWidth(), graphsPanel.getHeight(), chartInfo);
			}
			catch (Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Can't create PNG image of Chart.");
			}
			conn.close();
		}
	}
	
	
	//Humidity Chart Routine
	public void humidityChart () throws SQLException, ClassNotFoundException{
		try{
			graphsPanel.removeAll();
			dbConnect();
			stmt = conn.createStatement();
			String sqlHum = "SELECT timestamp, humidity FROM rpi_data_test WHERE id>=((SELECT MAX(id) FROM rpi_data_test)-40) ORDER BY id ASC;";   
			String sqlTrendHum = "select rpi_data_test.timestamp, trendHumData.trendHumValue from rpi_data_test inner join trendHumData on rpi_data_test.id=trendHumData.id;";
			JDBCCategoryDataset datasetHum = new JDBCCategoryDataset(conn, sqlHum);
			JDBCCategoryDataset datasetTrendHum = new JDBCCategoryDataset(conn,sqlTrendHum);
			JFreeChart humChart = ChartFactory.createLineChart("Humidity", "Timestamp", "Humidity, %", datasetHum, PlotOrientation.VERTICAL, true, true, true);
			System.out.println(datasetHum.getColumnCount());
			humChart.getTitle().setPaint(Color.BLUE);
			
			CategoryPlot plot = humChart.getCategoryPlot();
			CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();
			xAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
			plot.setRangeGridlinePaint(Color.blue);
			plot.setDomainGridlinesVisible(true);
			plot.setDomainGridlinePaint(Color.BLUE);
			LineAndShapeRenderer renderer = (LineAndShapeRenderer)plot.getRenderer();
			renderer.setBaseShapesVisible(true);
			
			plot.setDataset(1, datasetTrendHum);
			LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(true, false);
			renderer2.setSeriesPaint(0, Color.GREEN);
			renderer2.setBaseShapesVisible(false);
			plot.setRenderer(1, renderer2);
			
			
			ChartPanel frameHum = new ChartPanel(humChart);

			graphsPanel.add(frameHum);
			frameHum.setVisible(true);
			conn.close();
			frameHum.setSize(graphsPanel.getWidth(), graphsPanel.getHeight());
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
	}
	public void pressureChart () throws SQLException, ClassNotFoundException{
		try{
			
			graphsPanel.removeAll();
			dbConnect();
			stmt = conn.createStatement();
			String sqlPressure = "SELECT timestamp, pressure FROM rpi_data_test WHERE id>=((SELECT MAX(id) FROM rpi_data_test)-40) ORDER BY id ASC;";   
			String sqlTrendPres = "select rpi_data_test.timestamp, trendPresData.trendPresValue from rpi_data_test inner join trendPresData on rpi_data_test.id=trendPresData.id;";
			JDBCCategoryDataset datasetPres = new JDBCCategoryDataset(conn, sqlPressure);
			JDBCCategoryDataset datasetTrendPres = new JDBCCategoryDataset(conn, sqlTrendPres);
			JFreeChart pressChart = ChartFactory.createLineChart("Atmospheric pressure", "Timestamp", "Pressure, mmHg", datasetPres, PlotOrientation.VERTICAL, true, true, true);
			pressChart.getTitle().setPaint(Color.BLUE);
			CategoryPlot plot = pressChart.getCategoryPlot();
			CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();
			ValueAxis yAxis = plot.getRangeAxis();
			yAxis.setRange(700, 800);
			xAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
			plot.setRangeGridlinePaint(Color.blue);
			plot.setDomainGridlinesVisible(true);
			plot.setDomainGridlinePaint(Color.BLUE);
			LineAndShapeRenderer renderer = (LineAndShapeRenderer)plot.getRenderer();
			renderer.setBaseShapesVisible(true);
			
			plot.setDataset(1, datasetTrendPres);
			LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(true, false);
			renderer2.setSeriesPaint(0, Color.GREEN);
			renderer2.setBaseShapesVisible(false);
			plot.setRenderer(1, renderer2);
			
			ChartPanel framePress = new ChartPanel(pressChart);
			graphsPanel.add(framePress);
			framePress.setVisible(true);
			conn.close();
			framePress.setSize(graphsPanel.getWidth(), graphsPanel.getHeight());
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
	}
	public void temperatureChart () throws ClassNotFoundException, SQLException {
		try{
			graphsPanel.removeAll();
			dbConnect();
			stmt = conn.createStatement();
			String sqlTemp = "SELECT timestamp, temperature FROM rpi_data_test WHERE id>=((SELECT MAX(id) FROM rpi_data_test)-40) ORDER BY id ASC;";   
			String sqlTrendTemp = "select rpi_data_test.timestamp, trendTempData.trendTempValue from rpi_data_test inner join trendTempData on rpi_data_test.id=trendTempData.id;";
			JDBCCategoryDataset datasetTemp = new JDBCCategoryDataset(conn, sqlTemp);
			JDBCCategoryDataset datasetTrendTemp = new JDBCCategoryDataset(conn, sqlTrendTemp);
			JFreeChart tempChart = ChartFactory.createLineChart("Temperature", "Timestamp", "Temperature, *C", datasetTemp, PlotOrientation.VERTICAL, true, true, true);
			tempChart.getTitle().setPaint(Color.BLUE);
			CategoryPlot plot = tempChart.getCategoryPlot();
			CategoryAxis xAxis = (CategoryAxis)plot.getDomainAxis();
			
			plot.setRangeGridlinePaint(Color.blue);
			plot.setDomainGridlinesVisible(true);
			plot.setDomainGridlinePaint(Color.BLUE);
			LineAndShapeRenderer renderer = (LineAndShapeRenderer)plot.getRenderer();
			renderer.setBaseShapesVisible(true);
			
			plot.setDataset(1, datasetTrendTemp);
			LineAndShapeRenderer renderer2 = new LineAndShapeRenderer(true, false);
			renderer2.setSeriesPaint(0, Color.GREEN);
			renderer2.setBaseShapesVisible(false);
			plot.setRenderer(1, renderer2);
			xAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
			
			ChartPanel frameTemp = new ChartPanel(tempChart);
			graphsPanel.add(frameTemp);
			frameTemp.setVisible(true);
			conn.close();
			frameTemp.setSize(graphsPanel.getWidth(), graphsPanel.getHeight());
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	private void closeApplication() {
	        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        AppWindow.this.dispose();
	    }
	
	public void exitApplication() {
        // Opening of a JOptionPane to confirm Exit.
        int result = JOptionPane.showConfirmDialog(this, "Are you sure you want to exit the application?", "Exit RPI Monitor", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.closeApplication();
        }
    }
}
